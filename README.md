# Obesidad en adultos mayores

Análisis de la obesidad y sus factores en adultos mayores utilizando las 
encuestas ENASEM y ELSA

## Fases

Para el presente estudio se llevarán a cabo las siguientes actividades


- Elaboración de un manifiesto de datos de ENASEM
- Elaboración de un manifiesto de datos de ELSA 
- Carga de la ENASEM en PostgreSQL
- Carga de ELSA en PostgreSQL
- Revisión de variables relevantes ENASEM
- Revisión de variables relevantes ELSA
- Análisis de calidad de datos ENASEM
- Análisis de calidad de datos ELSA
- Generación del modelo ENASEM 2015
- Generación del modelo ELSA
- Generación del modelo ENASEM 2015 (dep) con 2012 (indep)

## Materiales

Se descargaron las siguientes encuestas:

- CRELES
- LASI
- KLoSE
- ENASEM
- ELSA

## Métodos

### Manifiesto y calidad de datos:

Se descargaron todos los datos diponibles por encuesta así como los
cuestionarios, estos datos fueron cargados en una base de datos SQLite 3.

llamada `obesity-db.sqlite`

Se generaron conteos para las variables de interés por encuesta:

- nulos
- registros totales

### Estadística descriptiva

Se calcularon los sigueintes indicadores por variable dependiendo de su tipo:

**Categóricas**

- conteos
- histograma
- frecuencias

**Numéricas**

- Media
- Mediana
- Moda
- Desviación estándar
- Skewness
- Kurtosis

### Modelación
Se hizo una regresión logística multinomial para ver que factores influyen en la
obesidad de adultos mayores en ambos países. Se generaron las probabilidades de
transición entre olas para ENASEM a partir de los datos 2012 a 2015

## Resultados


## Conclusiones
